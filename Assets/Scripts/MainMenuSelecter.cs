using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuSelecter : MonoBehaviour
{
    public bool inGame;
    public GameObject controls;
    public GameObject credits;
    public List<Image> menuItems;
    public List<Sprite> itemNormal;
    public List<Sprite> itemSelected;
    private int currentIndex;
    private float selectStart;
    private float selectTimer;
    // Start is called before the first frame update
    void Start()
    {
        selectStart = 0.15f;
        selectTimer = 0.0f;
        currentIndex = 0;
        menuItems[currentIndex].sprite = itemSelected[currentIndex];
    }

    // Update is called once per frame
    void Update()
    {
        if(inGame)
        {
            HandlePlayerOptions();
        }
        else
        {
            if (controls.activeSelf || credits.activeSelf)
            {
                if (Input.GetButtonDown("FrontArm") || Input.GetButtonDown("FrontArm-2") || Input.GetButtonDown("BackArm") || Input.GetButtonDown("BackArm-2"))
                {
                    controls.SetActive(false);
                    credits.SetActive(false);
                }
            }
            else
            {
                HandlePlayerOptions();
            }
        }
    }

    private void HandlePlayerOptions()
    {
        if (selectTimer <= 0.0f)
        {
            int x = (int)Input.GetAxisRaw("Horizontal");
            if (x == 0)
                x = (int)Input.GetAxisRaw("Horizontal2");
            if (x != 0)
            {
                menuItems[currentIndex].sprite = itemNormal[currentIndex];
                currentIndex += x;
                if (currentIndex >= menuItems.Count)
                    currentIndex = 0;
                else if (currentIndex < 0)
                    currentIndex = menuItems.Count - 1;
                menuItems[currentIndex].sprite = itemSelected[currentIndex];
                selectTimer = selectStart;
            }
        }
        else
            selectTimer -= Time.deltaTime;

        if (Input.GetButtonDown("FrontArm") || Input.GetButtonDown("FrontArm-2"))
        {
            if (currentIndex == 0)
            {
                if (inGame)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
                else
                {
                    //load vs scene
                    SceneManager.LoadScene("CombatScene");
                }
            }
            if (currentIndex == 1)
            {
                if (inGame)
                {
                    SceneManager.LoadScene("MainMenu");
                }
                else
                {
                    //quit the game
                    Application.Quit();
                }
            }
            if(currentIndex == 2)
            {
                SceneManager.LoadScene("BossScene");
            }
            if(currentIndex == 3)
            {
                credits.SetActive(true);
            }
            if (currentIndex == 4)
            {
                //display controls
                controls.SetActive(true);
            }
        }
    }
}
