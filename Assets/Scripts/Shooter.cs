using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    enum WeaponType
    {
        BASIC,
        HOMING
    }

    public Shooter otherArm;
    public HeatMeter heatMeter;
    public Transform rightShootPos;
    public Transform leftShootPos;
    public Transform playerTransform;
    public Animator playerAnim;
    private Transform shootPos;
    public GameObject basicShotPrefab;
    public float chargeTime = 0.0f;
    public bool isPlayer1;
    public bool isLeftArm;
    public float recoilForce;
    //private float chargeIncrease = 0.5f;
    //private int maxCharge = 3;
    private WeaponType currentWeapon = WeaponType.BASIC;
    private bool canShoot;
    private GameObject currentShot;

    private string armVal = "RightShoot";
    private Rigidbody2D playerRB;
    private Movement playerMove;

    private Vector2 recoil;
    private string FrontArm;
    // Start is called before the first frame update
    void Start()
    {
        canShoot = true;
        if (isLeftArm)
        {
            if (isPlayer1)
                FrontArm = "FrontArm";
            else
                FrontArm = "FrontArm-2";
            shootPos = rightShootPos;
            armVal = "LeftShoot";
        }
        else
        {
            if (isPlayer1)
                FrontArm = "BackArm";
            else
                FrontArm = "BackArm-2";
            shootPos = leftShootPos;
        }
        currentShot = null;
        playerRB = playerTransform.gameObject.GetComponent<Rigidbody2D>();
        playerMove = playerTransform.gameObject.GetComponent<Movement>();
        recoil = new Vector2(recoilForce, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (canShoot && currentShot == null)
        {
            if (Input.GetButton(FrontArm))
                heatMeter.Increase();
            if (Input.GetButtonUp(FrontArm))
            {
                heatMeter.Decrease();
                chargeTime = 0.0f;
                shootProjectile();
                canShoot = false;
                playerAnim.SetTrigger(armVal);
            }
        }
    }

    private void shootProjectile()
    {
        float direction = 1.0f;
        if (playerTransform.localScale.x < 0.0f)
            direction = -1.0f;
        switch (currentWeapon)
        {
            case WeaponType.BASIC:
                GameObject newBullet = Instantiate(basicShotPrefab) as GameObject;
                newBullet.transform.position = shootPos.position;
                newBullet.GetComponent<BasicShot>().applyChargeLevel(heatMeter.GetChargeLevel(), isPlayer1, direction);
                currentShot = newBullet;
                SoundManager.PlaySound("CreateProjectile");
                break;
            case WeaponType.HOMING:
                break;
        }
        if(gameObject.activeSelf)
            StartCoroutine(ShotRecoil(direction * -1.0f));
    }

    private IEnumerator ShotRecoil(float direction)
    {
        playerMove.ToggleMovement(true);
        playerRB.drag = 10.0f;
        playerRB.velocity = (recoil * direction);
        yield return new WaitForSecondsRealtime(0.5f);
        playerRB.drag = 0.0f;
        playerMove.ToggleMovement(false);
    }

    private void OnDisable()
    {
        if (Input.GetButton(FrontArm))
        {
            heatMeter.Decrease();
            chargeTime = 0.0f;
            shootProjectile();
            canShoot = false;
        }
    }

    public void EndCharge()
    {
        chargeTime = 0.0f;
        canShoot = false;
    }

    public void enableShot()
    {
        canShoot = true;
    }

    public void swapShootPos()
    {
        isLeftArm = !isLeftArm;
        if (isLeftArm)
        {
            if (isPlayer1)
                FrontArm = "FrontArm";
            else
                FrontArm = "FrontArm-2";
            //shootPos = rightShootPos;
        }
        else
        {
            if (isPlayer1)
                FrontArm = "BackArm";
            else
                FrontArm = "BackArm-2";
            //shootPos = leftShootPos;
        }
    }

    public void copyShooterData(Shooter otherShooter)
    {
        //swap arm-specific data. Like other 
        heatMeter = otherShooter.heatMeter;
        gameObject.SetActive(otherShooter.gameObject.activeSelf);
    }
}
