using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertProjectile : MonoBehaviour
{
    public GameObject explosionPrefab;
    public float speed;
    private Rigidbody2D rb2D;
    public bool isPlayer1 { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        rb2D.velocity = Vector2.down * speed;
    }

    public void applyChargeLevel(int chargeLevel)
    {
        speed *= chargeLevel;
        this.isPlayer1 = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            Destroy(this.gameObject);
        }
    }

    public void startDestruction(bool createExplosion = false)
    {
        //explode and destroy
        if (createExplosion)
        {
            GameObject explosion = Instantiate(explosionPrefab) as GameObject;
            explosion.transform.position = transform.position;
            explosion.transform.localScale = new Vector2(0.5f, 0.5f);
        }
        Destroy(this.gameObject);
    }

}
