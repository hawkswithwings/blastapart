using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject explosionPrefab;
    public BossTracker bossTracker;
    public float moveSpeed;

    public Transform topPos, bottomPos, shootPos;
    public GameObject hProj, vProj;
    public Animator bossAnim;

    private Rigidbody2D rb;

    // Decides when to launch vertical attacks (phase two)
    private bool launchVert, moveUp;
    
    // Time between firing vertical and horizontal attacks
    private float hp, maxHP, vertTimer, horiWait, horiTimer, vertRate, horiRate, hHigh, hLow, vLow, vHigh;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        // 12 hits kill the boss, every 4 hits trigger the next phase (3 phases)
        hp = 12;
        maxHP = 12;

        hHigh = 4.0f;
        hLow = 2.0f;

        vHigh = 4.0f;
        vLow = 2.0f;

        vertRate = Random.Range(vLow, vHigh);
        horiRate = Random.Range(hLow, hHigh);

        horiTimer = horiRate;
        vertTimer = vertRate;
        horiWait = 0;
        moveUp = true;
        launchVert = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(horiTimer > 0)
        {
            horiTimer -= Time.deltaTime;

            if(horiTimer <= 0)
            {
                horiWait = 0.5f;
                bossAnim.SetTrigger("Attack");
            }
        }
        if(horiWait > 0)
        {
            horiWait -= Time.deltaTime;
            if(horiWait <= 0)
            {
                ShootHori();
                horiRate = Random.Range(hLow, hHigh);
                horiTimer = horiRate;
            }
        }

        if(launchVert && vertTimer > 0)
        {
            vertTimer -= Time.deltaTime;
            if(vertTimer <= 0)
            {

                int choice = Random.Range(0, 3);

                switch (choice)
                {
                    case 0:
                        StartCoroutine(VertAttackOne());
                        break;
                    case 1:
                        StartCoroutine(VertAttackTwo());
                        break;
                    case 2:
                        StartCoroutine(VertAttackThree());
                        break;
                    case 3:
                        StartCoroutine(VertAttackFour());
                        break;
                }

               
            }
        }

        if(this.transform.position.y > topPos.position.y && moveUp)
            moveUp = false;
        if (this.transform.position.y < bottomPos.position.y && !moveUp)
            moveUp = true;
    }

    void FixedUpdate()
    {
        if (moveUp)
            rb.velocity = new Vector2(0, moveSpeed);
        else
            rb.velocity = new Vector2(0, moveSpeed * -1.0f);
    }

    public void DecreaseHP()
    {
        hp--;
        if(hp == maxHP - 4)
        {
            moveSpeed *= 1.5f;
            launchVert = true;
            hHigh *= 0.75f;
            hLow *= 0.75f;
            horiRate = Random.Range(hLow, hHigh);
        }

        else if(hp == maxHP - 8)
        {
            moveSpeed *= 1.5f;
            hHigh *= 0.75f;
            hLow *= 0.75f;
            horiRate = Random.Range(hLow, hHigh);
            vHigh *= 0.75f;
            vLow *= 0.75f;
            vertRate = Random.Range(vLow, vHigh);
        }

        else if(hp <= 0)
        {
            GameObject explosion = Instantiate(explosionPrefab) as GameObject;
            explosion.transform.position = transform.position;
            explosion.transform.localScale = new Vector2(2.5f, 2.5f);
            bossTracker.KnockedOut(false);
            gameObject.SetActive(false);
        }
    }

    private void ShootHori()
    {
        int choice = Random.Range(1, 3);

        float direction = -1.0f;
        GameObject newBullet = Instantiate(hProj) as GameObject;
        newBullet.transform.position = shootPos.position;
        newBullet.GetComponent<BasicShot>().applyChargeLevel(choice, false, direction);
        SoundManager.PlaySound("CreateProjectile");
    }

    private void ShootVert(Vector3 pos)
    {
        GameObject newBullet = Instantiate(vProj) as GameObject;
        newBullet.transform.position = pos;
        newBullet.GetComponent<VertProjectile>().applyChargeLevel(1);
        SoundManager.PlaySound("CreateProjectile");
    }


    private IEnumerator VertAttackOne()
    {
        float waitTime = 2.0f;

        // attack pattern goes from in front to back

        // Make first proj
        Vector3 pos = new Vector3(0.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make second
        pos = new Vector3(-2.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make third
        pos = new Vector3(-4.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);
        // make fourth

        pos = new Vector3(-6.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);
        // make fifth

        pos = new Vector3(-8.0f, 10.0f, 0.0f);
        ShootVert(pos);
        yield return new WaitForSecondsRealtime(waitTime);

        vertRate = Random.Range(vLow, vHigh);
        vertTimer = vertRate;
    }
    private IEnumerator VertAttackTwo()
    {
        //attack pattern goes from back to front

        float waitTime = 2.0f;

        // Make first proj
        Vector3 pos = new Vector3(-8.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make second
        pos = new Vector3(-6.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make third
        pos = new Vector3(-4.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);
        // make fourth

        pos = new Vector3(-2.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);
        // make fifth

        pos = new Vector3(0.0f, 10.0f, 0.0f);
        ShootVert(pos);
        yield return new WaitForSecondsRealtime(waitTime);

        vertRate = Random.Range(vLow, vHigh);
        vertTimer = vertRate;
    }
    private IEnumerator VertAttackThree()
    {
        float waitTime = 2.0f;


        //attack pattern goes from sides to middle

        // Make first two proj

        Vector3 pos = new Vector3(-8.0f, 10.0f, 0.0f);
        ShootVert(pos);

        pos = new Vector3(0.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make second two

        pos = new Vector3(-6.0f, 10.0f, 0.0f);
        ShootVert(pos);

        pos = new Vector3(-2.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make third singular

        pos = new Vector3(-4.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        vertRate = Random.Range(vLow, vHigh);
        vertTimer = vertRate;
    }

    private IEnumerator VertAttackFour()
    {
        float waitTime = 2.0f;
        //attack pattern goes from sides to middle

        // Make first middle
        Vector3 pos = new Vector3(-4.0f, 10.0f, 0.0f);
        ShootVert(pos);


        yield return new WaitForSecondsRealtime(waitTime);

        // make second two

        pos = new Vector3(-6.0f, 10.0f, 0.0f);
        ShootVert(pos);

        pos = new Vector3(-2.0f, 10.0f, 0.0f);
        ShootVert(pos);

        yield return new WaitForSecondsRealtime(waitTime);

        // make third outer

        pos = new Vector3(-8.0f, 10.0f, 0.0f);
        ShootVert(pos);

        pos = new Vector3(0.0f, 10.0f, 0.0f);
        ShootVert(pos);


        yield return new WaitForSecondsRealtime(waitTime);

        vertRate = Random.Range(vLow, vHigh);
        vertTimer = vertRate;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Projectile projectile = collision.gameObject.GetComponent<Projectile>();
            if (projectile.isPlayer1)
            {
                projectile.startDestruction(true);
                DecreaseHP();
            }
        }
    }
}
