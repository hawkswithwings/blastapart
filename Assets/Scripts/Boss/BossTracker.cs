using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossTracker : MonoBehaviour
{
    public GameObject pauseScreen;
    public GameObject playAgain;
    public List<HeatMeter> heatMeters;
    public GameObject player;
    public GameObject boss;
    public List<MonoBehaviour> components;
    public Animator blackFade;
    public ItemDropper itemDropper;
    public Image roundStart;
    public Text roundDisplay;

    private bool roundEnding;
    private bool canPause;

    // Start is called before the first frame update
    void Start()
    {
        roundEnding = false;
        canPause = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canPause && Input.GetButtonDown("Cancel"))
        {
            pauseScreen.SetActive(!pauseScreen.activeSelf);
            if (Time.timeScale > 0.0f)
                Time.timeScale = 0.0f;
            else
                Time.timeScale = 1.0f;
        }
    }

    public void BeginFight()
    {
        roundStart.enabled = false;
        for (int i = 0; i < components.Count; i++)
        {
            components[i].enabled = true;
        }
        canPause = true;
    }

    public void StartRoundAnimation()
    {
        for (int i = 0; i < components.Count; i++)
        {
            components[i].enabled = false;
        }
    }

    public void KnockedOut(bool isPlayer)
    {
        if (!roundEnding)
        {
            canPause = false;
            for (int i = 0; i < components.Count; i++)
            {
                components[i].enabled = false;
            }
            StartCoroutine(KnockoutSlowdown(isPlayer));
        }
    }

    private IEnumerator KnockoutSlowdown(bool isPlayer)
    {
        roundEnding = true;
        itemDropper.enabled = false;

        GameObject[] projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
        
        Time.timeScale = 0.5f;
        yield return new WaitForSecondsRealtime(1.0f);
        Time.timeScale = 1.0f;

        for (int i = 0; i < projectiles.Length; i++)
            Destroy(projectiles[i]);
        for (int i = 0; i < items.Length; i++)
            Destroy(items[i]);

        for (int i = 0; i < heatMeters.Count; i++)
            heatMeters[i].ResetValues();

        if (isPlayer) //The player sent the knocked out message, so they lose
        {
            roundDisplay.text = "Eyezor Destroys All";
        }
        else //boss sent knocked out message, so player wins!
        {
            roundDisplay.text = "Eyezor Defeated!!";
        }
        //display the replay and title options

        Time.timeScale = 1.0f;
        yield return new WaitForSecondsRealtime(2.0f);
        roundDisplay.text = "";
        blackFade.Play("FadeOut", 0);
        yield return new WaitForSecondsRealtime(0.75f);
        playAgain.SetActive(true);
        roundEnding = false;
    }
}
