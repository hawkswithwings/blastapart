using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatMeter : MonoBehaviour
{
    public RectTransform rect;
    public Shooter shooter;
    public Shaker shaker;
    public float steps;
    private float startPos;
    private float midPos;
    private float maxPos;
    private float increaseAmount;
    private bool increase;
    private Vector2 nextPos;
    private float fillSpeed;
    private float emptySpeed;
    private bool moving;

    // Start is called before the first frame update
    void Start()
    {
        moving = false;
        increase = false;
        maxPos = 0.0f;
        startPos = -rect.rect.width;
        midPos = startPos / 2;
        increaseAmount = rect.rect.width / steps;
        nextPos = new Vector2(startPos, 0.0f);
        transform.localPosition = nextPos;
        fillSpeed = 100.0f;
        emptySpeed = 25.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            if (increase)
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, nextPos, Time.deltaTime * fillSpeed);
                if (transform.localPosition.x == nextPos.x)
                {
                    nextPos.x += increaseAmount;
                    if (nextPos.x > maxPos)
                    {
                        nextPos.x = maxPos;
                        shaker.BeginShake();
                    }
                        
                }
            }
            else
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, nextPos, Time.deltaTime * emptySpeed);
                if (nextPos.x == transform.localPosition.x)
                {
                    nextPos.x -= increaseAmount;
                    if (nextPos.x < startPos)
                    {
                        nextPos.x = startPos;
                        moving = false;
                        shooter.enableShot();
                    }
                        
                }
            }
        }
        
    }

    public void Increase()
    {
        moving = true;
        increase = true;
    }
    public void Decrease()
    {
        moving = true;
        increase = false;
        shaker.EndShake();
    }

    public int GetChargeLevel()
    {
        if (transform.localPosition.x < midPos)
            return 1;
        else if (transform.localPosition.x < startPos)
            return 2;
        else
            return 3;
    }

    public void ForceDecrease()
    {
        moving = true;
        increase = false;
        shooter.EndCharge();
    }

    public void ResetValues()
    {
        shaker.EndShake();
        moving = false;
        increase = false;
        nextPos = new Vector2(startPos, 0.0f);
        transform.localPosition = nextPos;
    }
}
