using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropper : MonoBehaviour
{
    public GameObject itemPrefab;
    public List<GameObject> arms;
    public Transform player1Pos;
    public Transform player2Pos;

    private float minX, midX, maxX;
    private float ySpot;

    private float itemTimer;
    private int maxItemTime;
    private int minItemTime;

    private bool p1MissingArm;
    private bool p2MissingArm;
    private GameObject item;
    // Start is called before the first frame update
    void Start()
    {
        item = null;
        maxItemTime = 10;
        minItemTime = 2;
        itemTimer = Random.Range(minItemTime, maxItemTime);
        p1MissingArm = false;
        p2MissingArm = false;
        ySpot = itemPrefab.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(item == null)
        {
            itemTimer -= Time.deltaTime;
            if (itemTimer <= 0.0f)
            {
                itemTimer = Random.Range(minItemTime, maxItemTime);
                if (AreArmsMissing())
                {
                    minX = Camera.main.ScreenToWorldPoint(new Vector2(0.0f, 0.0f)).x + 1.0f;
                    maxX = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0.0f)).x - 1.0f;
                    midX = minX + ((maxX - minX) / 2);
                    float xSpot = 0.0f;
                    float xSpotLeft = Random.Range(minX, midX);
                    float xSpotRight = Random.Range(midX, maxX);
                    float xSpotAny = Random.Range(minX, maxX);
                    if (p1MissingArm && !p2MissingArm)
                    {
                        if (player1Pos.position.x < player2Pos.position.x)
                            xSpot = xSpotLeft;
                        else
                            xSpot = xSpotRight;
                    }
                    else if (!p1MissingArm && p2MissingArm)
                    {
                        if (player1Pos.position.x < player2Pos.position.x)
                            xSpot = xSpotRight;
                        else
                            xSpot = xSpotLeft;
                    }
                    else if (p1MissingArm && p2MissingArm)
                        xSpot = xSpotAny;
                    //Create item at random spot
                    Vector2 itemSpot = new Vector2(xSpot, ySpot);
                    item = Instantiate(itemPrefab) as GameObject;
                    item.transform.position = itemSpot;
                }
            }
        }
        
    }

    private bool AreArmsMissing()
    {
        p1MissingArm = false;
        p2MissingArm = false;
        for(int i = 0; i < arms.Count; i++)
        {
            if (arms[i].activeSelf == false)
            {
                if (i <= 1)
                    p1MissingArm = true;
                else
                    p2MissingArm = true;
            }
        }
        return p1MissingArm || p2MissingArm;
    }
}
