using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject explosionPrefab;
    public bool isPlayer1 { get; set; }
    public bool hitWeakPoint = false;

    public void FixedUpdate()
    {
        checkOffScreen();
    }

    private void checkOffScreen()
    {
        if (!GetComponent<Renderer>().isVisible)
            Destroy(this.gameObject);
    }

    public void startDestruction(bool createExplosion = false)
    {
        //explode and destroy
        if (createExplosion)
        {
            GameObject explosion = Instantiate(explosionPrefab) as GameObject;
            explosion.transform.position = transform.position;
            explosion.transform.localScale = new Vector2(0.5f, 0.5f);
        }
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("WeakPoint"))
            hitWeakPoint = true;
    }
}
