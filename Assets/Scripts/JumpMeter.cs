using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpMeter : MonoBehaviour
{
    public RectTransform rect;
    public Movement playerMovement;
    public float steps;
    private GameObject parentObj;
    private Transform playerTransform;
    private float startPos;
    private float maxPos;
    private float speed;
    private bool increasing;
    private Vector2 nextPos;
    private bool moving;
    private float xOffset;
    private Vector2 parentStart;

    public bool shake;
    private float startAmount;
    private float shakeAmount;
    private float shakeTime;
    // Start is called before the first frame update
    void Start()
    {
        increasing = false;
        moving = false;
        startPos = -rect.rect.height;
        maxPos = 0.0f;
        speed = playerMovement.startJumpTime * rect.rect.height;
        transform.localPosition = new Vector2(transform.localPosition.x, startPos);
        nextPos = new Vector2(transform.localPosition.x, maxPos);
        parentObj = transform.parent.gameObject;
        playerTransform = playerMovement.transform;
        xOffset = -0.5f;
        parentStart = parentObj.transform.position;
        parentObj.SetActive(false);

        shake = false;
        startAmount = 0.5f;
        shakeAmount = 0.5f;
        shakeTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        CalculateLocation();
        if (shake)
        {
            Vector3 tempPos = parentObj.transform.localPosition;
            float xShake = parentObj.transform.localPosition.x + Random.Range(-shakeAmount, shakeAmount);
            float yShake = parentObj.transform.localPosition.y + Random.Range(-shakeAmount, shakeAmount);
            parentObj.transform.localPosition = new Vector2(xShake, yShake);
            shakeTime += Time.deltaTime;
            if (shakeTime >= 1.5f)
            {
                shakeAmount += 1.0f;
                shakeTime = 0.0f;
                if (shakeAmount >= 2.0f)
                {
                    parentObj.transform.localPosition = tempPos;
                    playerMovement.ForceEndJump();
                    shake = false;
                    shakeAmount = startAmount;
                    SoundManager.PlaySound("Overheat");
                }
            }
        }
        else if (moving)
        {
            if (increasing)
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, nextPos, Time.deltaTime * speed);
                if (transform.localPosition.y == nextPos.y)
                {
                    nextPos.y = startPos;
                    increasing = false;
                    shake = true;
                }
            }
            else
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, nextPos, Time.deltaTime * speed);
                if (nextPos.y == transform.localPosition.y)
                {
                    nextPos.y = maxPos;
                    moving = false;
                    parentObj.SetActive(false);
                    parentObj.transform.position = parentStart;
                    playerMovement.CanJump();
                }
            }
        }
    }

    private void CalculateLocation()
    {
        float xMultiply = playerTransform.localScale.x;
        if (xMultiply < 0.0f)
            xMultiply = -1.0f;
        else
            xMultiply = 1.0f;
        parentObj.transform.position = Camera.main.WorldToScreenPoint(new Vector2(playerTransform.position.x +(xOffset * xMultiply), playerTransform.position.y));
    }

    public void StartJumpCharge()
    {
        nextPos.y = maxPos;
        parentObj.SetActive(true);
        moving = true;
        increasing = true;
    }

    public void EndJumpCharge()
    {
        nextPos.y = startPos;
        increasing = false;
        shake = false;
        shakeAmount = startAmount;
    }
}
