using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMusic : MonoBehaviour
{
    public GameObject controls;
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] music = GameObject.FindGameObjectsWithTag("Music");

        if (music.Length > 1)
        {
            controls.SetActive(false);
            Destroy(music[0]);
        }
        DontDestroyOnLoad(this.gameObject);
    }

}
