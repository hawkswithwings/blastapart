using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManagerNetwork : NetworkBehaviour
{
    public List<Transform> bones;
    public GameObject explosion;
    public Animator animator;
    public RoundTracker roundTracker;
    public BossTracker bossTracker;
    public List<GameObject> armA, armB;
    public float knockbackForce;


    private bool isPlayer1;
    private float invincible;
    private float invincibleTime;

    private string vertical;
    private Movement movementComp;
    private Rigidbody2D rb;
    private Vector2 knockback;
    private bool launched;
    private float startY;

    private int health;
    private bool triggerHappening;
    private List<Vector2> bonePos;
    // Start is called before the first frame update
    void Start()
    {
        isPlayer1 = isLocalPlayer;
        health = 3;
        movementComp = gameObject.GetComponent<Movement>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        invincibleTime = 0.5f;
        invincible = 0.0f;
        if (isPlayer1)
            vertical = "Vertical";
        else
            vertical = "Vertical2";

        knockback = new Vector2(knockbackForce, knockbackForce * 2.0f);
        if (transform.localScale.x > 0.0f)
            knockback.x *= -1.0f;
        launched = false;
        triggerHappening = false;

        bonePos = new List<Vector2>();
        for (int i = 0; i < bones.Count; i++)
            bonePos.Add(bones[i].localPosition);
    }

    // Update is called once per frame
    void Update()
    {
        if (invincible > 0.0f && !launched)
        {
            invincible -= Time.deltaTime;
            if (invincible < 0.0f)
                invincible = 0.0f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (triggerHappening == false)
        {
            triggerHappening = true;
            if (collision.gameObject.tag == "Projectile")
            {
                Projectile projectile = collision.gameObject.GetComponent<Projectile>();
                if (projectile != null)
                {
                    if (projectile.isPlayer1 != isPlayer1)
                    {
                        bool hitWeakPoint = projectile.hitWeakPoint;
                        projectile.startDestruction(invincible > 0.0f);
                        if (invincible <= 0.0f)
                        {
                            if (hitWeakPoint)
                                health = 0;
                            else
                                health -= 1;
                            if (health <= 0)
                                health = 0;
                            StartCoroutine(knockBack());
                        }
                    }
                }
                else
                {
                    VertProjectile vertProjectile = collision.gameObject.GetComponent<VertProjectile>();
                    if (vertProjectile != null)
                    {
                        if (vertProjectile.isPlayer1 != isPlayer1)
                        {
                            vertProjectile.startDestruction(invincible > 0.0f);
                            if (invincible <= 0.0f)
                            {
                                health -= 1;
                                if (health <= 0)
                                    health = 0;
                                startDestruction();
                                if (gameObject.activeSelf)
                                    StartCoroutine(knockBack());
                            }
                        }
                    }
                }
            }
            triggerHappening = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Item") && Input.GetAxisRaw(vertical) < 0.0f && movementComp.IsGrounded())
        {
            health += 1;
            if (health > 3)
                health = 3;
            Destroy(collision.gameObject);
            if (armB[0].activeSelf == false)
            {
                for (int i = 0; i < armB.Count; i++)
                    armB[i].SetActive(true);
            }
            else if (armA[0].activeSelf == false)
            {
                for (int i = 0; i < armA.Count; i++)
                    armA[i].SetActive(true);
            }
        }
    }

    private void startDestruction()
    {
        movementComp.ForceEndJumpFromHit();
        if (health == 2)
        {
            GameObject armExplode = Instantiate(explosion) as GameObject;
            armExplode.transform.parent = armA[0].transform.parent;
            armExplode.transform.localPosition = armA[0].transform.localPosition;
            for (int i = 0; i < armA.Count; i++)
                armA[i].SetActive(false);
        }
        else if (health == 1)
        {
            GameObject armExplode = Instantiate(explosion) as GameObject;
            armExplode.transform.parent = armB[0].transform.parent;
            armExplode.transform.localPosition = armB[0].transform.localPosition;
            for (int i = 0; i < armB.Count; i++)
                armB[i].SetActive(false);
        }
        else
        {
            GameObject bodyExplode = Instantiate(explosion) as GameObject;
            Vector2 explodePos = transform.position;
            explodePos.y += 2.0f;
            bodyExplode.transform.localScale = bodyExplode.transform.localScale * 2.0f;
            bodyExplode.transform.position = explodePos;
        }
    }

    private IEnumerator knockBack()
    {
        startDestruction();
        if (health == 0)
        {
            animator.SetTrigger("Hit");
            yield return new WaitForSecondsRealtime(0.1f);
            gameObject.SetActive(false);
            ResetBones();
            if (roundTracker != null)
                roundTracker.KnockedOut(isPlayer1);
            else if (bossTracker != null)
                bossTracker.KnockedOut(true);
        }
        else
        {
            invincible = invincibleTime;
            movementComp.ToggleMovement(true);
            knockback.x = knockbackForce;
            if (transform.localScale.x > 0.0f)
                knockback.x *= -1.0f;
            rb.drag = 10.0f;
            rb.velocity = knockback;
            launched = true;
            animator.SetTrigger("Hit");
            yield return new WaitForSecondsRealtime(0.45f);
            rb.velocity = Vector2.zero;
            rb.drag = 0.0f;
            launched = false;
            movementComp.ToggleMovement(false);
        }
    }

    public int GetHealth()
    {
        return health;
    }

    public void ResetHealth()
    {
        health = 3;
    }

    private void ResetBones()
    {
        for (int i = 0; i < bones.Count; i++)
            bones[i].localPosition = bonePos[i];
    }
}
