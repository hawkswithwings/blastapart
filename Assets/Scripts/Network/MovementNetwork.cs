using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementNetwork : NetworkBehaviour
{
    public JumpMeter jumpMeter;
    public Transform other;
    public bool positionalFlip = true;
    
    public float groundSpeed, airSpeed, jumpSpeed, dashSpeed, grAccel, grDecel, airAccel, airDecel, glideFall, startJumpTime, dashTime;
    public bool flipped;
    public AudioSource chargeAudio;

    public Transform TCheck, GCheck, FCheck, BCheck;
    public LayerMask GLayer;

    public Animator playerAnim;

    public bool disableMovement;
    private Rigidbody2D rb;
    private bool grounded, startJump, gliding, canDash, startDash;
    private Vector2 moveInput, dashInput;
    private float curSpeed, curJumpSp, jumpTimer, dashTimer;

    private string jump, dash, horizontal, vertical;
    private bool canJump, chargingJump;

    private bool isPlayer1;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        canJump = true;
        moveInput = new Vector2();
        grounded = false;
        gliding = false;
        startJump = false;
        canDash = false;

        jumpTimer = 0;
        dashTimer = 0;

        isPlayer1 = isLocalPlayer;

        if (isPlayer1)
        {
            jump = "Jump";
            dash = "Dash";
            horizontal = "Horizontal";
            vertical = "Vertical";
        }
        else
        {
            jump = "Jump2";
            dash = "Dash-2";
            horizontal = "Horizontal2";
            vertical = "Vertical2";
        }
        disableMovement = false;
        chargingJump = false;
    }

    // Update is called once per frame
    void Update()
    {
        JumpInput();

        if (dashTimer > 0)
        {
            dashTimer -= Time.deltaTime;
            if (dashTimer <= 0)
            {
                dashTimer = 0;
                rb.drag = 0;
            }
        }

        if (chargingJump)
        {
            jumpTimer -= Time.deltaTime;
            if (jumpTimer <= 0)
            {
                // Decide what percentage to jump by
                jumpTimer = 0;
                curJumpSp = jumpSpeed;
                //startJump = true;
            }

        }

        else
        {
            MoveInput();
        }

        if (other != null)
        {
            if ((other.position.x > transform.position.x && transform.localScale.x < 0.0f) || (other.position.x < transform.position.x && transform.localScale.x > 0.0f))
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                flipped = !flipped;
            }
        }
    }

    private void OnDisable()
    {
        EndAllMovement();
    }

    private void OnEnable()
    {
        bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.02f, GLayer);
        if (gCheck)
        {
            //LandLagTimer = LandLag;
            grounded = gCheck;
            gliding = false;
            canDash = true;
        }
    }

    private void FixedUpdate()
    {
        MoveMe();
    }
    private void LateUpdate()
    {
        if (!disableMovement)
            playerAnim.SetFloat("moveVal", Input.GetAxis(horizontal) * this.transform.localScale.x);
    }

    private void MoveInput()
    {
        if (disableMovement)
            return;
        float h = Input.GetAxis(horizontal);
        float v = Input.GetAxis(vertical);
        float h1 = Input.GetAxisRaw(horizontal);
        float v1 = Input.GetAxisRaw(vertical);

        if (grounded)
        {
            if (positionalFlip == false)
            {
                if (h < 0 && !flipped)
                {
                    transform.localScale = new Vector3(-0.75f, transform.localScale.y, transform.localScale.z);
                    flipped = true;
                }

                if (h > 0 && flipped)
                {
                    transform.localScale = new Vector3(0.75f, transform.localScale.y, transform.localScale.z);
                    flipped = false;
                }
            }

            HandleSpeed(h, groundSpeed, grAccel, grDecel);
        }

        else
        {
            HandleSpeed(h, airSpeed, airAccel, airDecel);
        }

        moveInput = new Vector2(curSpeed, v);


        if (Input.GetButtonDown(dash) && canDash)
        {

            //rb.drag = 10;
            //rb.velocity = Vector2.zero;


            //rb.velocity += new Vector2(0, 1) * dashSpeed;
            if (v1 == 0 && h1 == 0)
                dashInput = new Vector2(0, 1).normalized * dashSpeed;
            else
                dashInput = new Vector2(h1, v1).normalized * dashSpeed;
            //rb.velocity += new Vector2(h1, v1) * dashSpeed;
            dashTimer = dashTime;
            startDash = true;
            canDash = false;
            gliding = false;
            SoundManager.PlaySound("Jump");
        }
    }

    void HandleSpeed(float h, float target, float accel, float decel)
    {
        if (h > 0.0f)
        {
            if (curSpeed < target * h)
                curSpeed += accel;
            else if (curSpeed > target * h)
                curSpeed = target;

        }
        else if (h < 0.0f)
        {
            if (curSpeed > target * h)
                curSpeed -= accel;
            else if (curSpeed < target * h)
                curSpeed = -target;

        }
        else if (h == 0.0f)
        {
            if (curSpeed > 0.0f)
            {
                curSpeed -= decel;
                if (curSpeed < 0.0f)
                    curSpeed = 0.0f;
            }
            else if (curSpeed < 0.0f)
            {
                curSpeed += decel;
                if (curSpeed > 0.0f)
                    curSpeed = 0.0f;
            }
        }

    }
    private void JumpInput()
    {
        if (disableMovement || !canJump)
            return;
        if (Input.GetButtonDown(jump))
        {
            if (grounded && jumpTimer == 0)
            {
                chargingJump = true;
                jumpTimer = startJumpTime;
                curJumpSp = jumpSpeed;
                if(jumpMeter != null)
                    jumpMeter.StartJumpCharge();
                playerAnim.SetTrigger("StartJump");
                //SoundManager.PlaySound("Charging");
                chargeAudio.Play();
            }

            else if (rb.velocity.y < 0 && !gliding)
            {
                gliding = true;
            }
        }

        if (Input.GetButtonUp(jump))
        {
            if (chargingJump)
            {
                if (jumpTimer > 0.0f)
                {
                    curJumpSp = ((startJumpTime - jumpTimer) / startJumpTime) * jumpSpeed;
                    if (curJumpSp < jumpSpeed / 2)
                        curJumpSp = jumpSpeed / 2;
                }
                jumpTimer = 0.0f;
                startJump = true;
                chargingJump = false;
                if(jumpMeter != null)
                    jumpMeter.EndJumpCharge();
            }

            if (gliding && rb.velocity.y < 0)
            {
                gliding = false;
            }
        }


    }

    public void ForceEndJump()
    {
        chargingJump = false;
        jumpTimer = 0.0f;
        canJump = false;
        jumpMeter.EndJumpCharge();
        playerAnim.SetTrigger("Jump");
    }

    public void ForceEndJumpFromHit()
    {
        chargingJump = false;
        jumpTimer = 0.0f;
        jumpMeter.EndJumpCharge();
    }

    private void MoveMe()
    {
        if (disableMovement)
            return;

        if (dashTimer <= 0)
            rb.velocity = new Vector2(moveInput.x, rb.velocity.y);

        // Currently, just make 'em jump. Add more weight later
        if (startJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, curJumpSp);
            playerAnim.SetTrigger("Jump");
            chargeAudio.Stop();
            SoundManager.PlaySound("Jump");
            startJump = false;
        }

        if (gliding)
        {
            rb.velocity = new Vector2(moveInput.x, glideFall);
        }
        if (startDash)
        {
            rb.drag = 10;
            rb.velocity = Vector2.zero;
            rb.velocity += dashInput;
            startDash = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.02f, GLayer);
            bool tCheck = Physics2D.OverlapCircle(TCheck.position, 0.02f, GLayer);
            bool fCheck = Physics2D.OverlapCircle(FCheck.position, 0.02f, GLayer);
            bool bCheck = Physics2D.OverlapCircle(BCheck.position, 0.02f, GLayer);

            if (gCheck)
            {
                //LandLagTimer = LandLag;
                grounded = gCheck;
                gliding = false;
                canDash = true;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.05f, GLayer);
            bool fCheck = Physics2D.OverlapCircle(FCheck.position, 0.05f, GLayer);
            bool bCheck = Physics2D.OverlapCircle(BCheck.position, 0.05f, GLayer);

            if (!gCheck)
            {
                grounded = false;

            }
        }
    }

    public float GetJumpTimer()
    {
        return jumpTimer;
    }

    public void ToggleMovement(bool moveValue)
    {
        disableMovement = moveValue;
        if (!disableMovement)
        {
            EndAllMovement();
        }
        else
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.02f, GLayer);
            if (gCheck)
            {
                //LandLagTimer = LandLag;
                grounded = gCheck;
                gliding = false;
                canDash = true;
            }
        }
    }
    public bool IsGrounded()
    {
        return grounded;
    }

    private void EndAllMovement()
    {
        playerAnim.SetFloat("moveVal", 0.0f);
        rb.velocity = Vector2.zero;
        gliding = false;
        dashTimer = 0.0f;
    }

    public void CanJump()
    {
        canJump = true;
        if (Input.GetButton(jump) && grounded)
        {
            chargingJump = true;
            jumpTimer = startJumpTime;
            curJumpSp = jumpSpeed;
            jumpMeter.StartJumpCharge();
            playerAnim.SetTrigger("StartJump");
            chargeAudio.Play();
        }
    }
}
