using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicShotNetwork : BasicProjNetwork
{
    public float speed;
    private Rigidbody2D rb2D;
    private float direction = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public new void FixedUpdate()
    {
        rb2D.velocity = Vector2.right * speed * direction;
        base.FixedUpdate();
    }

    public void applyChargeLevel(int chargeLevel, bool isPlayer1, float direction)
    {
        speed *= chargeLevel;
        this.isPlayer1 = isPlayer1;
        if (direction < 0)
        {
            this.direction = direction;
            transform.Rotate(Vector3.up, 180.0f);
        }
    }
}
