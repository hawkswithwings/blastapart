using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManagerBA : NetworkManager
{
    public Transform playerOneSpawn;
    public Transform playerTwoSpawn;
    public GameObject player2Prefab;

    // Start is called before the first frame update
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // add player at correct spawn position
        Transform start = numPlayers == 0 ? playerOneSpawn : playerTwoSpawn;
        GameObject prefab = numPlayers == 0 ? playerPrefab : player2Prefab;
        GameObject player = Instantiate(prefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);

        // spawn ball if two players
        if (numPlayers == 2)
        {
           // Start the round counter
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }
}
