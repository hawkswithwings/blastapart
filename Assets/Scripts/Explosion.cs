using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float timeOnScreen = 1.0f;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = timeOnScreen;
        SoundManager.PlaySound("Hit");
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0.0f)
            Destroy(this.gameObject);
    }
}
