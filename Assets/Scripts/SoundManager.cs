using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip createProj, hit, jump, overheat;
    public static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        createProj = Resources.Load<AudioClip>("CreateProjectile");
        hit = Resources.Load<AudioClip>("Hit");
        jump = Resources.Load<AudioClip>("Jump");
        overheat = Resources.Load<AudioClip>("Overheat");


        audioSource = this.GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {

            case "CreateProjectile":
                audioSource.PlayOneShot(createProj);
                break;

            case "Hit":
                audioSource.PlayOneShot(hit);
                break;

            case "Jump":
                audioSource.PlayOneShot(jump);
                break;

            case "Overheat":
                audioSource.PlayOneShot(overheat);
                break;
        }
    }


}
