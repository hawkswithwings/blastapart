using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundTracker : MonoBehaviour
{
    public GameObject playAgain;
    public Text countdownText;
    public int victoryNum;
    public List<HeatMeter> heatMeters;
    public GameObject player1;
    public GameObject player2;
    public Text p1Rounds;
    public Text p2Rounds;
    public Image roundStart;
    public List<MonoBehaviour> components;
    public List<GameObject> arms;
    public Animator blackFade;
    public Text roundDisplay;
    public ItemDropper itemDropper;

    public GameObject controls;

    public int maxRoundTime;

    private int p1Wins;
    private int p2Wins;
    private Vector2 p1Start;
    private Vector2 p2Start;
    private int currentRound;
    private int currentRoundTime;
    private float roundTime;
    private bool countdown;
    private HealthManager p1Health, p2Health;

    private bool roundEnding;

    // Start is called before the first frame update
    void Start()
    {
        p1Start = player1.transform.position;
        p2Start = player2.transform.position;
        currentRound = 1;
        roundTime = 1.0f;
        currentRoundTime = maxRoundTime;
        countdown = false;

        p1Health = player1.GetComponent<HealthManager>();
        p2Health = player2.GetComponent<HealthManager>();

        countdownText.text = "" + currentRoundTime;
        roundEnding = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown)
        {
            roundTime -= Time.deltaTime;
            if(roundTime <= 0.0f)
            {
                roundTime = 1.0f;
                currentRoundTime--;
                string extraZero = "";
                if (currentRoundTime < 10)
                    extraZero = "0"; 
                string cdText = extraZero + currentRoundTime;
                countdownText.text = cdText;
                if (currentRoundTime <= 0)
                {
                    countdown = false;
                    if(!roundEnding)
                        StartCoroutine(KnockoutSlowdown(true));
                }
            }
        }


        if (Input.GetButtonDown("Cancel") && countdown)
        {
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                controls.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                controls.SetActive(true);
            }
        }

    }

    public void BeginFight()
    {
        roundStart.enabled = false;
        for (int i = 0; i < components.Count; i++)
        {
            components[i].enabled = true;
        }
        countdown = true;
    }

    public void StartRoundAnimation()
    {
        for (int i = 0; i < components.Count; i++)
        {
            components[i].enabled = false;
        }
    }

    public void KnockedOut(bool player1KnockedOut)
    {
        if (!roundEnding)
        {
            for (int i = 0; i < components.Count; i++)
            {
                components[i].enabled = false;
            }
            StartCoroutine(KnockoutSlowdown(player1KnockedOut));
        }
    }

    private void UpdateP1Wins()
    {
        string p1 = "";
        p1Wins++;
        for(int i = 0; i < p1Wins; i++)
        {
            p1 += "I";
        }
        p1Rounds.text = p1;
    }

    private void UpdateP2Wins()
    {
        string p2 = "";
        p2Wins++;
        for (int i = 0; i < p2Wins; i++)
        {
            p2 += "I";
        }
        p2Rounds.text = p2;
    }

    private IEnumerator KnockoutSlowdown(bool player1KnockedOut)
    {
        roundEnding = true;
        itemDropper.enabled = false;
        bool draw = p1Health.GetHealth() == p2Health.GetHealth();

        countdown = false;
        GameObject[] projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
        if (draw)
        {
            roundDisplay.text = "DRAW";
            for (int i = 0; i < components.Count; i++)
                components[i].enabled = false;
            player1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            for (int i = 0; i < projectiles.Length; i++)
                Destroy(projectiles[i]);
            for (int i = 0; i < items.Length; i++)
                Destroy(items[i]);
            yield return new WaitForSecondsRealtime(2.0f);

            roundDisplay.text = "";
        }
        
        if (!draw)
        {
            Time.timeScale = 0.5f;
            yield return new WaitForSecondsRealtime(1.0f);
            Time.timeScale = 1.0f;

            if (player1KnockedOut)
                UpdateP2Wins();
            else
                UpdateP1Wins();

            for (int i = 0; i < projectiles.Length; i++)
                Destroy(projectiles[i]);
            for (int i = 0; i < items.Length; i++)
                Destroy(items[i]);
        }

        for (int i = 0; i < heatMeters.Count; i++)
            heatMeters[i].ResetValues();

        if(p2Wins >= victoryNum || p1Wins >= victoryNum)
        {
            string playerNum = "1";
            if (p2Wins >= victoryNum)
                playerNum = "2";
            roundDisplay.text = "Player " + playerNum + " wins!!";
            Time.timeScale = 1.0f;
            yield return new WaitForSecondsRealtime(2.0f);
            roundDisplay.text = "";
            blackFade.Play("FadeOut", 0);
            yield return new WaitForSecondsRealtime(0.75f);
            playAgain.SetActive(true);
        }
        else
        {
            blackFade.Play("FadeOut", 0);
            yield return new WaitForSecondsRealtime(0.75f);
            ResetPlayers();
            currentRoundTime = maxRoundTime;
            roundTime = 1.0f;
            countdownText.text = "" + currentRoundTime;
            blackFade.Play("FadeIn", 0);
            yield return new WaitForSecondsRealtime(0.5f);
            currentRound++;
            roundDisplay.text = "ROUND " + currentRound;
            yield return new WaitForSecondsRealtime(1.0f);
            roundDisplay.text = "Commence!!";
            yield return new WaitForSecondsRealtime(1.0f);
            roundDisplay.text = "";
            for (int i = 0; i < components.Count; i++)
            {
                components[i].enabled = true;
                if (components[i] is Shooter)
                    ((Shooter)components[i]).enableShot();
            }
            countdown = true;
            p1Health.ResetHealth();
            p2Health.ResetHealth();
            itemDropper.enabled = true;
        }
        roundEnding = false;
    }

    private void ResetPlayers()
    {
        for (int i = 0; i < components.Count; i++)
        {
            components[i].enabled = false;
            components[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < arms.Count; i++)
        {
            arms[i].SetActive(true);
        }
        player1.transform.position = p1Start;
        player2.transform.position = p2Start;
        player1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player1.GetComponent<Rigidbody2D>().drag = 0;
        player2.GetComponent<Rigidbody2D>().drag = 0;
        if (player1.transform.localScale.x < 0)
        {
            Vector3 scaleValue = player1.transform.localScale;
            scaleValue.x *= -1.0f;
            player1.transform.localScale = scaleValue;
        }
        if (player2.transform.localScale.x > 0)
        {
            Vector3 scaleValue = player2.transform.localScale;
            scaleValue.x *= -1.0f;
            player2.transform.localScale = scaleValue;
        }
    }
}
