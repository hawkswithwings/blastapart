using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmSwitcher : MonoBehaviour
{
    public ArmActions armA;
    public ArmActions armB;
    public Animator playerAnim;
    public List<MonoBehaviour> otherComponents;
    public bool isPlayer1;
    private string BackArm;
    private float swapTime;
    private float swapCountdown;

    // Start is called before the first frame update
    void Start()
    {
        swapCountdown = 0.0f;
        swapTime = 0.677f;
        if (isPlayer1)
        {
            BackArm = "Dash";
        }
        else
            BackArm = "Dash-2";
    }

    // Update is called once per frame
    void Update()
    {
        if (swapCountdown > 0.0f)
        {
            swapCountdown -= Time.deltaTime;
            if(swapCountdown <= 0.0f)
            {
                for (int i = 0; i < otherComponents.Count; i++)
                {
                    otherComponents[i].enabled = true;
                }
                armA.copyArm(armB.GetComponent<Shooter>());
                armB.copyArm(armA.GetComponent<Shooter>());
            }
        }
        else
        {
            if (Input.GetButtonDown(BackArm))
            {
                for (int i = 0; i < otherComponents.Count; i++)
                {
                    otherComponents[i].enabled = false; //turn off Shooter components
                }
                playerAnim.SetTrigger("ArmSwap");
                swapCountdown = swapTime;
                /*if(leftArm.gameObject.activeSelf)
                    leftArm.beginSwitch(rightArmPos.localPosition);
                if(rightArm.gameObject.activeSelf)
                    rightArm.beginSwitch(leftArmPos.localPosition);*/
            }
        }
        
    }
}
