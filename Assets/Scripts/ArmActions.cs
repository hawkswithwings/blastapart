using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmActions : MonoBehaviour
{
    public bool isPlayer1;
    private Shooter shooter;
    // Start is called before the first frame update
    void Start()
    {
        shooter = gameObject.GetComponent<Shooter>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Projectile")
        {
            if(collision.gameObject.GetComponent<Projectile>().isPlayer1 != isPlayer1)
            {
                collision.gameObject.GetComponent<Projectile>().startDestruction();
                startDestruction();
            }
        }
    }

    private void startDestruction()
    {
        //play explosion and destroy
        gameObject.SetActive(false);
    }
    public void copyArm(Shooter otherArm)
    {
        shooter.swapShootPos();
    }


}
