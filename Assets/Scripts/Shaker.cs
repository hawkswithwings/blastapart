using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour
{
    public HeatMeter heatMeter;
    public bool shake;
    private Vector2 startPos;
    private float startAmount;
    private float amount;
    private float shakeTime;

    // Start is called before the first frame update
    void Start()
    {
        shake = false;
        startPos = transform.localPosition;
        startAmount = 0.5f;
        amount = 0.5f;
        shakeTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (shake)
        {
            float xShake = startPos.x + Random.Range(-amount, amount);
            float yShake = startPos.y + Random.Range(-amount, amount);
            transform.localPosition = new Vector2(xShake, yShake);
            shakeTime += Time.deltaTime;
            if(shakeTime >= 0.5f)
            {
                amount += 1.0f;
                shakeTime = 0.0f;
                if (amount >= 2.0f)
                {
                    transform.localPosition = startPos;
                    heatMeter.ForceDecrease();
                    shake = false;
                    amount = startAmount;
                    SoundManager.PlaySound("Overheat");
                }
            }
        }
    }

    public void BeginShake()
    {
        shake = true;
    }

    public void EndShake()
    {
        transform.localPosition = startPos;
        shake = false;
        amount = 0.5f;
        shakeTime = 0.0f;
    }
}
